# SpringBoot整合Aspectj-LTW（Load Time Weaving）技术

使用Aspectj-LTW会**在 JVM 进行类加载的时候进行织入**，下面讲演示SpringBoot整合Aspectj-LTW。

### 1、导入maven依赖和aspectjweaver.jar

[aspectjweaver.jar下载链接：https://mirrors.ustc.edu.cn/eclipse/tools/aspectj/aspectj-1.8.14.jar](https://mirrors.ustc.edu.cn/eclipse/tools/aspectj/aspectj-1.8.14.jar)

[aspectjweaver.jar下载页面：https://www.eclipse.org/aspectj/downloads.php](https://www.eclipse.org/aspectj/downloads.php)

导入依赖：

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>

<dependency>
    <groupId>aspectj</groupId>
    <artifactId>aspectjweaver</artifactId>
    <version>1.5.3</version>
</dependency>
```

### 2、创建切面类和需要被切入的目标类

示例项目目录结构：

<img src="imgs/image-20210706145938443.png" alt="image-20210706145938443" style="zoom: 80%;" />

**TestAspect.java**

```java
@Slf4j
@Aspect
public class TestAspect {
    @Pointcut("execution(* com.shph.demo.test..*.*(..))|| @annotation(java.lang.Deprecated)")
    public void pointCut() {
    }

    @Before("pointCut()")
    public void before(JoinPoint point) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        log.info(String.format("%s\t%s", Modifier.toString(signature.getModifiers()), signature.getName()));

    }
}
```

**HelloTestImpl.java**

```java
@Slf4j
@Service
public class HelloTestImpl implements HelloTest {
    @Deprecated
    @Override
    public void test01() {
        log.info("------------test01-----------");

        test02();

        test03();

        test04();
    }

    @Deprecated
    private static void test04() {
        log.info("------------test04-----------");
    }

    @Deprecated
    private void test03() {
        log.info("------------test03-----------");
    }

    @Deprecated
    @Override
    public void test02() {
        log.info("------------test02-----------");
    }
}
```

### 3、配置aop.xml

需要注意的是aop.xml需要在`	META-INF/aop.xml`路径下

**aop.xml**

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<aspectj>
    <weaver options="-XnoInline -Xset:weaveJavaxPackages=true -Xlint:ignore -verbose -XmessageHandlerClass:org.springframework.aop.aspectj.AspectJWeaverMessageHandler">
        <!--在编织时导入切面类和需要被切入的目标类-->
        <include within="com.shph.demo.test..*"/>
        <include within="com.shph.demo.aspect..*"/>
    </weaver>
    <aspects>
        <!--指定切面类-->
        <aspect name="com.shph.demo.aspect.TestAspect"/>
    </aspects>
</aspectj>
```

### 4、在程序运行时，需要配置**VM**参数

![image-20210706153403827](imgs/image-20210706153403827.png)

```shell
# -javaagent:aspectjweaver.jar的路径
-javaagent:src/main/resources/aspectjweaver.jar
```

