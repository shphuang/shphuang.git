package com.shph.demo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Modifier;

@Slf4j
@Aspect
public class TestAspect {
    @Pointcut("execution(* com.shph.demo.test..*.*(..))||@annotation(java.lang.Deprecated)")
    public void pointCut() {
    }

    @Before("pointCut()")
    public void before(JoinPoint point) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        log.info(String.format("%s\t%s", Modifier.toString(signature.getModifiers()), signature.getName()));

    }
}
