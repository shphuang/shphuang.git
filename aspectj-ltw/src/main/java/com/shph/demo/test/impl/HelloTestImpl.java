package com.shph.demo.test.impl;

import com.shph.demo.test.HelloTest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HelloTestImpl implements HelloTest {
    @Deprecated
    @Override
    public void test01() {
        log.info("------------test01-----------");

        test02();

        test03();

        test04();
    }

    @Deprecated
    private static void test04() {
        log.info("------------test04-----------");
    }

    @Deprecated
    private void test03() {
        log.info("------------test03-----------");
    }

    @Deprecated
    @Override
    public void test02() {
        log.info("------------test02-----------");
    }
}
