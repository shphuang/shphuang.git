package com.example.demo.model;

import lombok.Data;

//返回实体封装
@Data
public class Result {
    private int status = 200;

    private String msg = "success";

    private String error;

    private Object data;

    private Result() {
    }

    public static Result success(String msg, Object data) {
        Result result = new Result();
        result.setMsg(msg);
        result.setData(data);
        return result;
    }

    public static Result success() {
        Result result = new Result();
        return result;
    }

    public static Result success(String msg) {
        Result result = new Result();
        result.setMsg(msg);
        return result;
    }

    public static Result success(Object data) {
        Result result = new Result();
        result.setData(data);
        return result;
    }


    public static Result error(String msg, String error) {
        Result result = new Result();
        result.setStatus(500);
        result.setMsg(msg);
        result.setError(error);
        return result;
    }

    public static Result error(String error) {
        Result result = new Result();
        result.setStatus(500);
        result.setError(error);
        return result;
    }
}
