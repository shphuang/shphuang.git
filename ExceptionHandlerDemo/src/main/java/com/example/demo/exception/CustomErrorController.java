package com.example.demo.exception;

import com.example.demo.model.Result;
import com.example.demo.utils.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
public class CustomErrorController implements ErrorController {

    @Autowired
    ErrorAttributes errorAttributes;

    @Autowired
    ServerProperties serverProperties;

    @Autowired
    @Qualifier("errorView")
    View view;


    @RequestMapping
    public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = getErrorAttributes(request, getErrorAttributeOptions(request, MediaType.ALL));
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(view);

        if (model == null || model.isEmpty()) {
            return modelAndView;
        }

        Result error = Result.error((String) model.get("message"), (String) model.get("error"));
        error.setStatus((Integer) model.get("status"));

        Map<String, Object> map = MapUtils.toMap(error);
        modelAndView.addAllObjects(map);
        return modelAndView;
    }


    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public ModelAndView mediaTypeNotAcceptable(HttpServletRequest request) {
        Map<String, Object> model = getErrorAttributes(request, getErrorAttributeOptions(request, MediaType.ALL));
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(view);

        if (model == null || model.isEmpty()) {
            return modelAndView;
        }

        Result error = Result.error((String) model.get("message"), (String) model.get("error"));
        error.setStatus((Integer) model.get("status"));

        Map<String, Object> map = MapUtils.toMap(error);
        modelAndView.addAllObjects(map);
        return modelAndView;
    }


    @Override
    public String getErrorPath() {
        return null;
    }

    protected Map<String, Object> getErrorAttributes(HttpServletRequest request, ErrorAttributeOptions options) {
        WebRequest webRequest = new ServletWebRequest(request);
        return this.errorAttributes.getErrorAttributes(webRequest, options);
    }

    protected ErrorAttributeOptions getErrorAttributeOptions(HttpServletRequest request, MediaType mediaType) {
        ErrorAttributeOptions options = ErrorAttributeOptions.defaults();

        /*ErrorProperties errorProperties = serverProperties.getError();
        if (errorProperties.isIncludeException()) {
            options = options.including(ErrorAttributeOptions.Include.EXCEPTION);
        }
        if (isIncludeStackTrace(request, mediaType)) {
            options = options.including(ErrorAttributeOptions.Include.STACK_TRACE);
        }
        if (isIncludeMessage(request, mediaType)) {
            options = options.including(ErrorAttributeOptions.Include.MESSAGE);
        }
        if (isIncludeBindingErrors(request, mediaType)) {
            options = options.including(ErrorAttributeOptions.Include.BINDING_ERRORS);
        }*/

        return options;
    }

}
