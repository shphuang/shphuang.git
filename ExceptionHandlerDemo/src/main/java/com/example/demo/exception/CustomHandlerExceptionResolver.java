package com.example.demo.exception;

import com.example.demo.model.Result;
import com.example.demo.utils.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class CustomHandlerExceptionResolver implements HandlerExceptionResolver, Ordered {
    @Autowired
    @Qualifier("errorView")
    View view;

    // 设置优先级最高，由该异常解析器解析异常处理
    @Override
    public int getOrder() {
        return Integer.MIN_VALUE;
    }

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        Result error = Result.error("程序异常，请联系管理员！");
        // 处理业务异常
        if (ex instanceof BusinessException) {
            error.setError(ex.getMessage());
        }

        Map<String, Object> model = MapUtils.toMap(error);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(view);
        modelAndView.addAllObjects(model);
        return modelAndView;
    }
}
