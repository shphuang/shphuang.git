package com.example.demo.exception;

//处理业务异常
public class BusinessException extends RuntimeException {
    private BusinessException(String message) {
        super(message);
    }

    public static BusinessException error(String error) {
        BusinessException exception = new BusinessException(error);
        return exception;
    }
}
