package com.example.demo.utils;

import com.alibaba.fastjson.JSON;

import java.util.Map;

public class MapUtils {
    /*
     * 将对象转换成map
     * */
    public static <T> Map<String, Object> toMap(T obj) {
        if (obj instanceof Map) {
            return (Map<String, Object>) obj;
        }
        String json = JSON.toJSONString(obj);
        return JSON.parseObject(json, Map.class);
    }
}
