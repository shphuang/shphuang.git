package com.example.demo.controller;

import com.example.demo.exception.BusinessException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @GetMapping("/test")
    public Object test(@RequestParam(value = "flag", defaultValue = "false") boolean flag) {
        if (flag) {
            throw BusinessException.error("业务异常信息");
        }
        return "成功";
    }

    @PostMapping("/post")
    public Object post(@RequestParam(value = "flag", defaultValue = "false") boolean flag) {
        if (flag) {
            throw BusinessException.error("业务异常信息");
        }
        return "成功";
    }
}
